# Greens 2016 Email Signature Generator #

Front-end code to generate email signatures in the Australian Greens 2016 style. 

Note: this repository contains snippets of code, which are inserted into a Drupal 7 node on the [Australian Greens Members' Website, Greenhouse](https://members.greens.org.au). 

### Potential improvements ###
* Ensure code is correctly indented and follows basic standards
* Ensure code passes appropriate linters
* Tweak output to work better on more platforms and devices
* Refactor generation using a templating system (e.g. Mustache)
* Provide multiple output formats, and clearer instructions for setting up the signature in a wide variety of clients