jQuery.fn.form = function() {
    var formData = {};
    this.find('[name]').each(function() {
        formData[this.name] = this.value;
    })
    return formData;
};

function makeSignature() {

    var values = jQuery("#signature").form();

    var html = '<p id="GreensSignature">';
    html += '<span id="name" style="text-transform: uppercase; font-size: 24px; color: #009639; font-weight: 700; font-family: \'Roboto Condensed\', \'Roboto\', \'Arial Narrow\', Arial, sans-serif;">';
    html += values["name"];
    html += '</span>';
    html += '<br>';
    html += '<span id="title" style="font-size: 18px; color: #009639; font-family: \'Roboto Condensed\', \'Roboto\', \'Arial Narrow\', Arial, sans-serif;">';
    html += values["title"];
    if (values["faction"]) {
        html += ' — ';
        html += values["faction"];
    }
    html += '</span>';
    html += '<br>';
    html += '<br>';
    html += '<span id="address" style="font-size: 12px; color: #009639; font-family: \'Roboto Condensed\', \'Roboto\', \'Arial Narrow\', Arial, sans-serif;">';
    html += values["address"];
    html += '</span>';
    html += '<br>';
    html += '<span id="phone" style="font-size: 12px; color: #009639; font-family: \'Roboto Condensed\', \'Roboto\', \'Arial Narrow\', Arial, sans-serif;">';
    html += values["phone"];
    if (values["skype"]) {
        html += ' | <a style="color: #009639; text-decoration: none; font-family: \'Roboto Condensed\', \'Roboto\', \'Arial Narrow\', Arial, sans-serif;"  href="skype:' + values["skype"] + '">SKP: ';
        html += values["skype"];
        html += '</a>';
    }
    if (values["facebook"]) {
        html += ' | <a style="color: #009639; text-decoration: none;font-family: \'Roboto Condensed\', \'Roboto\', \'Arial Narrow\', Arial, sans-serif;"  href="http://fb.me/' + values["facebook"] + '">FB: fb.me/';
        html += values["facebook"];
        html += '</a>';
    }
    if (values["twitter"]) {
        html += ' | <a style="color: #009639; text-decoration: none;font-family: \'Roboto Condensed\', \'Roboto\', \'Arial Narrow\', Arial, sans-serif;"  href="http://twitter.com/' + values["twitter"] + '">TW: ';
        html += values["twitter"];
        html += '</a>';
    }
    html += '</span>';
    html += '<br>';
    html += '<br>';
    if (values["web"]) {
        html += '<span id="web" style="font-size: 18px; color: #009639; font-family: \'Roboto Condensed\', \'Roboto\', \'Arial Narrow\', Arial, sans-serif;">';
        html += '<a style="font-size: 18px; color: #009639; font-family: \'Roboto Condensed\', \'Roboto\', \'Arial Narrow\', Arial, sans-serif;" href="http://';
        html += values["web"];
        html += '">';
        html += values["web"]
        html += '</a>';
        html += '</span>';
        html += '<br>'
    }
    if (values["image"]) {
        html += '<br><img src="';
        html += values["image"];
        html += '">';
    }
    html += '</p>';

    jQuery("#outputhtml").html(html);

    jQuery("#outputtext").text(html);

}